import unittest
import requests
import json

class TestData(unittest.TestCase):
	SITE_URL = 'http://student04.cse.nd.edu:52063'
	EDUCATION_URL = SITE_URL + '/education/'
	RESET_URL = SITE_URL + '/reset/'

	def reset_data(self):
		d = {}
		r = requests.put(self.RESET_URL, data = json.dumps(d))

	def is_json(self,resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_education_id_get(self):
		self.reset_data()
		ent_id = "High_School"
		r = requests.get(self.EDUCATION_URL + ent_id)
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['want'], 47.12)
		self.assertEqual(resp['attempt'], 60.4)
		self.assertEqual(resp['smoke'], 13.08)
		self.assertEqual(resp['smokeless'], 7.13)

	def test_education_id_delete(self):
		self.reset_data()
		ent_id = "High_School"

		d = {}
		r = requests.delete(self.EDUCATION_URL + ent_id, data = json.dumps(d))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.EDUCATION_URL + ent_id)
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['want'], 'no data')
		self.assertEqual(resp['attempt'], 'no data')
		self.assertEqual(resp['smoke'], 'no data')
		self.assertEqual(resp['smokeless'], 'no data')

	def test_eduation_get(self):
		self.reset_data()
		r = requests.get(self.EDUCATION_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())

		educations = resp['educations']

		for edu in educations:
			if edu['education'] == "Middle School":
				testdata = edu

		self.assertEqual(testdata['want'], 50.15)
		self.assertEqual(testdata['attempt'], 69.75)
		self.assertEqual(testdata['smoke'], 4.92)
		self.assertEqual(testdata['smokeless'], 2.87)

if __name__ == '__main__':
	unittest.main()



