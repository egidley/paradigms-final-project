import unittest
import requests
import json

class TestData(unittest.TestCase):
	SITE_URL = 'http://student04.cse.nd.edu:52063'
	GENDER_URL = SITE_URL + '/gender/'
	RESET_URL = SITE_URL + '/reset/'

	def reset_data(self):
		d = {}
		r = requests.put(self.RESET_URL, data = json.dumps(d))

	def is_json(self,resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_gender_id_get(self):
		self.reset_data()
		ent_id = "Female"
		r = requests.get(self.GENDER_URL + ent_id)
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['want'], 46.86)
		self.assertEqual(resp['attempt'], 59.32)
		self.assertEqual(resp['smoke'], 8.17)
		self.assertEqual(resp['smokeless'], 2.33)

	def test_gender_id_delete(self):
		self.reset_data()
		ent_id = "Female"

		d = {}
		r = requests.delete(self.GENDER_URL + str(ent_id), data = json.dumps(d))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.GENDER_URL + str(ent_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['want'], 'no data')
		self.assertEqual(resp['attempt'], 'no data')
		self.assertEqual(resp['smoke'], 'no data')
		self.assertEqual(resp['smokeless'], 'no data')

	def test_gender_get(self):
		self.reset_data()
		r = requests.get(self.GENDER_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())

		genders = resp['genders']

		for gen in genders:
			if gen['gender'] == "Male":
				testdata = gen

		self.assertEqual(testdata['want'], 44.34)
		self.assertEqual(testdata['attempt'], 62.05)
		self.assertEqual(testdata['smoke'], 9.43)
		self.assertEqual(testdata['smokeless'], 7.49)

if __name__ == '__main__':
	unittest.main()


