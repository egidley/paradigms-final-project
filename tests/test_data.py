import unittest
import requests
import json

class TestData(unittest.TestCase):
	SITE_URL = 'http://student04.cse.nd.edu:52063'
	DATA_URL = SITE_URL + '/data/'
	RESET_URL = SITE_URL + '/reset/'

	def reset_data(self):
		d = {}
		r = requests.put(self.RESET_URL, data = json.dumps(d))

	def is_json(self,resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_data_id_get(self):
		self.reset_data()
		ent_id = 10
		r = requests.get(self.DATA_URL + str(ent_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['education'], 'Middle School')
		self.assertEqual(resp['gender'], 'Overall')
		self.assertEqual(resp['locationdesc'], 'Arizona')

	def test_data_id_put(self):
		self.reset_data()
		ent_id = 3

		r = requests.get(self.DATA_URL + str(ent_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['education'], 'Middle School')
		self.assertEqual(resp['sample_size'], '679')
		self.assertEqual(resp['gender'], 'Male')

		d = {}
		d['education'] = 'High School'
		d['sample_size'] = '700'
		d['gender'] = 'Female'
		d['data_value'] = '11.0'
		r = requests.put(self.DATA_URL + str(ent_id), data = json.dumps(d))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.DATA_URL + str(ent_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['education'], d['education'])
		self.assertEqual(resp['sample_size'], d['sample_size'])
		self.assertEqual(resp['gender'], d['gender'])

	def test_data_id_delete(self):
		self.reset_data()
		ent_id = 4

		d = {}
		r = requests.delete(self.DATA_URL + str(ent_id), data = json.dumps(d))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')
		
		r = requests.get(self.DATA_URL + str(ent_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'error')

	def test_data_get(self):
		self.reset_data()
		r = requests.get(self.DATA_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())

		data = resp['data']
		#print(data)
		for d in data:
			if d['id'] == 3:
				testdata = d

		self.assertEqual(testdata['gender'],'Male')
		self.assertEqual(testdata['sample_size'], '679')

	def test_data_post(self):
		self.reset_data()

		d = {}
		d['gender'] = 'Female'
		d['sample_size'] = '250'
		d['data_value'] = '10.0'
		r = requests.post(self.DATA_URL, data = json.dumps(d))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		self.assertEqual(resp['id'], 874)

		r = requests.get(self.DATA_URL + str(resp['id']))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['gender'], d['gender'])
		self.assertEqual(resp['sample_size'], d['sample_size'])

	def test_data_delete(self):
		self.reset_data()

		d = {}
		r = requests.delete(self.DATA_URL, data = json.dumps(d))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'],'success')

		r = requests.get(self.DATA_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		data = resp['data']
		self.assertFalse(data)

if __name__ == '__main__':
	unittest.main()
