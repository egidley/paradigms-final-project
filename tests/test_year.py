import unittest
import requests
import json

class TestData(unittest.TestCase):
	SITE_URL = 'http://student04.cse.nd.edu:52063'
	YEAR_URL = SITE_URL + '/year/'
	RESET_URL = SITE_URL + '/reset/'

	def reset_data(self):
		d = {}
		r = requests.put(self.RESET_URL, data = json.dumps(d))

	def is_json(self,resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_year_id_get(self):
		self.reset_data()
		year = "2017"
		r = requests.get(self.YEAR_URL + year)
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['want'], 51.14)

	def test_year_id_delete(self):
		self.reset_data()
		year = 4

		d = {}
		r = requests.delete(self.YEAR_URL + str(year), data = json.dumps(d))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.YEAR_URL + str(year))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')

	def test_year_get(self):
		self.reset_data()
		r = requests.get(self.YEAR_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())

		self.assertEqual(resp['2015 want'], 46.05)
		self.assertEqual(resp['2017 attempt'], 61.38)
		self.assertEqual(resp['2016 smoke'], 8.50)
		self.assertEqual(resp['2015 smokeless'], 3.74)


if __name__ == '__main__':
	unittest.main()

