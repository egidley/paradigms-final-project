import unittest
import requests
import json

class TestData(unittest.TestCase):
	SITE_URL = 'http://student04.cse.nd.edu:52063'
	STATE_URL = SITE_URL + '/state/'
	RESET_URL = SITE_URL + '/reset/'

	def reset_data(self):
		d = {}
		r = requests.put(self.RESET_URL, data = json.dumps(d))

	def is_json(self,resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_state_id_get(self):
		self.reset_data()
		ent_id = "Indiana"
		r = requests.get(self.STATE_URL + ent_id)
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['want'], 'no data')
		self.assertEqual(resp['attempt'], 'no data')
		self.assertEqual(resp['smoke'], 4.02)
		self.assertEqual(resp['smokeless'], 0.85)

	def test_state_id_delete(self):
		self.reset_data()
		ent_id = 4

		d = {}
		r = requests.delete(self.STATE_URL + str(ent_id), data = json.dumps(d))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.STATE_URL + str(ent_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['want'], 'no data')
		self.assertEqual(resp['attempt'], 'no data')
		self.assertEqual(resp['smoke'], 'no data')
		self.assertEqual(resp['smokeless'], 'no data')

	def test_state_get(self):
		self.reset_data()
		r = requests.get(self.STATE_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())

		states = resp['states']

		for st in states:
			if st['state'] == "Alabama":
				testdata = st

		self.assertEqual(testdata['want'], 29.82)
		self.assertEqual(testdata['attempt'], 58.1)
		self.assertEqual(testdata['smoke'], 11.78)
		self.assertEqual(testdata['smokeless'], 8.39)

if __name__ == '__main__':
	unittest.main()

