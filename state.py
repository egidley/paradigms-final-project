import cherrypy
import re, json
from _tobacco_database import _tobacco_database

class StateController(object):
	def __init__(self, tdb=None):
		if tdb is None:
			self.tdb = _movie_database()
		else:
			self.tdb = tdb

		self.tdb.load_data('data.json')

	def GET_ALL(self):
		output = {"result" : "success"}
		states = ["Alabama","Alaska","Arizona","Arkansas","California","Colorado",
		"Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois",
		"Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland",
		"Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana",
		"Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York",
		"North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania",
		"Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah",
		"Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]

		l = list() #Initialize a list to store a dict for each state

		#Iterate through the states and calculate the average percentages for each measurement type
		for st in states:
			stw = self.tdb.filter_state_val(st, "want")
			sta = self.tdb.filter_state_val(st, "attempt")
			sts = self.tdb.filter_state_val(st, "smoke")
			stsl = self.tdb.filter_state_val(st, "smokeless")

			q_dicts = [stw, sta, sts, stsl] #List of the dictionarys corresponding to each measurement type
			questions = ["want", "attempt", "smoke", "smokeless"] #List of each measurement type

			tot = it = avg = itr = 0
			d = dict() #Dict to store statistics for each state
			d["state"] = st

			#Iterate through each dict corresponding to the different measurement types and calculate/store the average value
			for q in q_dicts:
				if q:
					for k,v in q.items():
						tot = tot + v
						it += 1
					avg = tot/it
					d[questions[itr]] = round(avg, 2)
				else:
					d[questions[itr]] = "no data"
				itr += 1
				tot = it = avg = 0			

			#Add to the list of states
			l.append(d)

		output["states"] = l
		return json.dumps(output)

	def GET_ID(self, ent_id):
		output = {"result" : "success"}
		ent_id = str(ent_id.replace("_"," "))
		# The URL may contain additional filters in the form of /state/Alabama:gender=Female:year=2016
		stack = ent_id.split(":") # List of each filter and its value
		st = stack[0] # The state name is the first argument
		del stack[0] # Delete once it's stored

		try:
			stw = self.tdb.filter_state_val(st, "want")
			sta = self.tdb.filter_state_val(st, "attempt")
			sts = self.tdb.filter_state_val(st, "smoke")
			stsl = self.tdb.filter_state_val(st, "smokeless")

			# Additionally call filter functions for each of the stacked filter types in order to select the targeted group
			for ids in stack:
				filters = ids.split("=")

				if filters[0] == "gender":
					stw = self.tdb.filter_gender_val(filters[1], "want", stw)
					sta = self.tdb.filter_gender_val(filters[1], "attempt", sta)
					sts = self.tdb.filter_gender_val(filters[1], "smoke", sts)
					stsl = self.tdb.filter_gender_val(filters[1], "smokeless", stsl)
				if filters[0] == "education":
					f = filters[1]	
					stw = self.tdb.filter_education_val(f, "want", stw)
					sta = self.tdb.filter_education_val(f, "attempt", sta)
					sts = self.tdb.filter_education_val(f, "smoke", sts)
					stsl = self.tdb.filter_education_val(f, "smokeless", stsl)
				if filters[0] == "year":
					f = int(filters[1])
					stw = self.tdb.filter_year_val(f, "want", stw)
					sta = self.tdb.filter_year_val(f, "attempt", sta)
					sts = self.tdb.filter_year_val(f, "smoke", sts)
					stsl = self.tdb.filter_year_val(f, "smokeless", stsl)

			q_dicts = [stw, sta, sts, stsl]
			questions = ["want", "attempt", "smoke", "smokeless"]

			tot = it = avg = itr = 0
			output["state"] = st

			for q in q_dicts:
				if q:
					for k,v in q.items():
						tot = tot + v
						it += 1
					avg = tot/it
					output[questions[itr]] = round(avg, 2)
				else:
					output[questions[itr]] = "no data"
				itr += 1
				tot = it = avg = 0

		except Exception as ex:
			output["result"] = "error"
			output["message"] = str(ex)

		return json.dumps(output)

	def DELETE_ID(self, ent_id):
		output = {"result" : "success"}
		ent_id = str(ent_id)
		filters = ent_id.split(":")
		st = filters[0].replace("_"," ") # Extract the state id and delete all of its entries

		try:
			states = self.tdb.filter_state_val(st) #Generates a list of the ids corresponding to the state
			if states:
				for k in states:
					self.tdb.delete_entry(k) #Delete all entries associated with the specified state
		except Exception as ex:
			output["result"] = "error"
			output["message"] = str(ex)

		return json.dumps(output)
