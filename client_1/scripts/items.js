function Div(){
	this.createDiv = function(className,id){
		this.item = document.createElement("div");
		this.item.setAttribute("id",id);
		this.item.classList.add(className);
	},
	this.addToDiv = function(element){
		this.item.appendChild(element.item);
	}
}

function Item(){
	this.addToDocument = function(){
		document.body.appendChild(this.item);
	}
}

function Label(){
	this.createLabel = function(text,id){
		this.item = document.createElement("p");
		this.item.setAttribute("id",id);
		this.item.innerText = text;
	},
	this.setText = function(text){
		this.item.innerText = text;
	}
}

function Button(){
	this.createButton = function(text,id){
		this.item = document.createElement("button");
		this.item.setAttribute("id",id);
		this.item.innerText = text;
	},
	this.addClickEventHandler = function(handler,args){
		this.item.onmouseup = function(){
			handler(args);
		}
	}
}

function Dropdown(){
	this.createDropdown = function(dict,id,selected){
		this.item = document.createElement("select");
		this.item.setAttribute("id",id);
		this.item.innerHTML = dict;
		for(var key in dict){
			var option = document.createElement("option");
			option.value = key;
			option.text = dict[key];
			if(key == selected){
				option.setAttribute("selected","true");
			}
			this.item.appendChild(option);
		}
	},
	this.getSelected = function(){
		return this.item.options[this.item.selectedIndex].value;
	}
}

function Image(){
    this.createImg = function(loc, id){
        this.item = document.createElement("img");
        this.item.setAttribute("id", id);   
        this.item.src = loc;
    },
    this.setImg = function(loc){
        this.item.src = loc;
    }
}
