//main.js

/*
Args[0]: State
Args[1]: Gender
Args[2]: Year
Args[3]: Education
Args[4]: Question want, attempt, smoke, smokeless
*/

function getInfo(arg) {
    //get selected values from dropdown
    var args = [arg[0].getSelected(), arg[1].getSelected(), arg[2].getSelected(), arg[3].getSelected(), arg[4].getSelected()]
    var xhr = new XMLHttpRequest();
    var endingstring = "";
    var datastring = "";
    var index = 0;
    var types = [ "state", "gender", "year", "education" ];
    //check which dropdowns are selected to find path
    while (index < 4){
        if (args[index] != "None"){
            endingstring = "/" + types[index] + "/" + args[index];
            datastring = args[index];
            index += 1;
            break;
        }
        index += 1;
    }
    while (index < 4){
        if (args[index] != "None"){
            endingstring += (":" + types[index] + "=" + args[index]);
            datastring += (", " + args[index]);
        }
        index += 1;
    }

    //add selected filters to server url
    xhr.open("GET", "http://student04.cse.nd.edu:52063" + String(endingstring) , true);

    xhr.onload = function(e){
        console.log(xhr.responseText);
        var data = JSON.parse(xhr.responseText);
        //display trend map and data prompt based on selected question type
        if (args[4] == "want"){
            mainLabel.innerHTML = "Those who wanted to quit (" + datastring + "): ";
            arg[6].setImg("../images/Want.png");
        }
        
        if (args[4] == "attempt"){
            mainLabel.innerHTML = "Those who attempted to quit (" + datastring + "): ";
            arg[6].setImg("../images/Attempt.png");
        }

        if (args[4] == "smoke"){
            mainLabel.innerHTML = "Those who smoked (" + datastring + "): ";
            arg[6].setImg("../images/Smoke.png");
        }

        if (args[4] == "smokeless"){
            mainLabel.innerHTML = "Those who used smokeless (" + datastring + "): ";
            arg[6].setImg("../images/Smokeless.png");
        }

        if (data[args[4]] == 'no data'){
            mainLabel.innerHTML += 'no data';
        }
        else {
            mainLabel.innerHTML += (data[args[4]] + "%");
        }
        arg[5].setText(mainLabel.innerHTML);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }
    xhr.send(null);
}

Div.prototype = new Item();
//create main container div
div = new Div();
div.createDiv("container", "Container");

//create title div
titleDiv = new Div();
titleDiv.createDiv("titleL", "titelDiv");
div.addToDiv(titleDiv);

//create divs for each dropdown
stateDiv = new Div();
stateDiv.createDiv("state", "StateDiv");
div.addToDiv(stateDiv);

genDiv = new Div();
genDiv.createDiv("gender", "GenderDiv");
div.addToDiv(genDiv);

eduDiv = new Div();
eduDiv.createDiv("edu", "EduDiv");
div.addToDiv(eduDiv);

yearDiv = new Div();
yearDiv.createDiv("year", "YearDiv");
div.addToDiv(yearDiv);

qDiv = new Div();
qDiv.createDiv("question", "qDiv");
div.addToDiv(qDiv);

//add title
titleLabel = new Label();
titleLabel.createLabel("Tobacco Usage in America", "titleLabel")
titleDiv.addToDiv(titleLabel);

//state dropdown
stateLabel = new Label();
stateLabel.createLabel("State:", "statelabel")
stateDiv.addToDiv(stateLabel);

stateDrop = new Dropdown();
stateDrop.createDropdown({
	"None" : "---",
    "Alabama": "Alabama",
    "Alaska": "Alaska",
    "Arizona": "Arizona",
    "Arkansas": "Arkansas",
    "California": "California",
    "Colorado": "Colorado",
    "Connecticut": "Connecticut",
    "Delaware": "Delaware",
    "Florida": "Florida",
    "Georgia": "Georgia",
    "Hawaii": "Hawaii",
    "Idaho": "Idaho",
    "Illinois": "Illinois",
    "Indiana": "Indiana",
    "Iowa": "Iowa",
    "Kansas": "Kansas",
    "Kentucky": "Kentucky",
    "Louisiana": "Louisiana",
    "Maine": "Maine",
    "Maryland": "Maryland",
    "Massachusetts": "Massachusetts",
    "Michigan": "Michigan",
    "Minnesota": "Minnesota",
    "Mississippi": "Mississippi",
    "Missouri": "Missouri",
    "Montana": "Montana",
    "Nebraska": "Nebraska",
    "Nevada": "Nevada",
    "New_Hampshire": "New Hampshire",
    "New_Jersey": "New Jersey",
    "New_Mexico": "New Mexico",
    "New_York": "New York",
    "North_Carolina": "North Carolina",
    "North_Dakota": "North Dakota",
    "Ohio": "Ohio",
    "Oklahoma": "Oklahoma",
    "Oregon": " Oregon",
    "Pennsylvania": "Pennsylvania",
    "Rhode_Island": "Rhode Island",
    "South_Carolina": "South Carolina",
    "South_Dakota": "South Dakota",
    "Tennessee": "Tennessee",
    "Texas": "Texas",
    "Utah": "Utah",
    "Vermont": "Vermont",
    "Virginia": "Virginia",
    "Washington": "Washington",
    "West_Virgina": "West Virginia",
    "Wisconsin": "Wisconsin",
    "Wyoming": "Wyoming"}, "stateDropdown", "None")
stateDiv.addToDiv(stateDrop);

//gender dropdown
genLabel = new Label();
genLabel.createLabel("Gender:", "genlabel")
genDiv.addToDiv(genLabel);

genderDrop = new Dropdown();
genderDrop.createDropdown({
	"None" : "---",
	"Female" : "Female",
	"Male" : "Male"}, "genDropdown", "None")
genDiv.addToDiv(genderDrop);

//education dropdown
eduLabel = new Label();
eduLabel.createLabel("Education Level:", "edulabel")
eduDiv.addToDiv(eduLabel);

eduDrop = new Dropdown();
eduDrop.createDropdown({
	"None" : "---",
	"Middle_School" : "Middle School",
	"High_School" : "High School"}, "eduDropdown", "None")
eduDiv.addToDiv(eduDrop);

//year dropdown
yearLabel = new Label();
yearLabel.createLabel("Year:", "yearlabel")
yearDiv.addToDiv(yearLabel);

yearDrop = new Dropdown();
yearDrop.createDropdown({
	"None" : "---",
	"2015" : "2015",
	"2016" : "2016",
	"2017" : "2017"}, "yearDropdown", "None")
yearDiv.addToDiv(yearDrop);

//question drop down
qLabel = new Label();
qLabel.createLabel("Data Question:", "qlabel")
qDiv.addToDiv(qLabel);

qDrop = new Dropdown();
qDrop.createDropdown({
	"want" : "Want to Quit",
	"attempt" : "Attempt to Quit",
	"smoke" : "Smoking",
	"smokeless" : "Smokeless"}, "qDropdown", "want")
qDiv.addToDiv(qDrop);

//create div for data display
dataDiv = new Div();
dataDiv.createDiv("data", "DataDiv");
div.addToDiv(dataDiv);

mainLabel = new Label();
mainLabel.createLabel("", "datalabel")
dataDiv.addToDiv(mainLabel);

//image initialization
imgDiv = new Div();
imgDiv.createDiv("img", "ImgDiv");

image = new Image();
image.createImg("../images/Want.png", "theImage");
imgDiv.addToDiv(image);

//create div for button
buttDiv = new Div();
buttDiv.createDiv("button", "ButtDiv");

button = new Button();
button.createButton("Filter", "theButton");

//creating click event
args = [stateDrop, genderDrop, yearDrop, eduDrop, qDrop, mainLabel, image]
button.addClickEventHandler(getInfo, args);
buttDiv.addToDiv(button);
div.addToDiv(buttDiv);

div.addToDiv(imgDiv);

div.addToDocument();
