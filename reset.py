#rest.py
#Paradigms Final Project

import cherrypy
import json

from _tobacco_database import _tobacco_database

class ResetController(object):
    def __init__(self, tdb=None):
        if tdb is None:
            self.tdb = _tobacco_database()
        else:
            self.tdb = tdb

    def RESET_DATA(self):               #resets all data
        output = {'result':'success'}

        try:
            self.tdb.load_data('data.json') #reloads data
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def RESET_ID(self,ent_id):          #resets data for a specific id
        output = {'result':'success'}
        ent_id = int(ent_id)

        try:
            tdb_tmp = _tobacco_database()       #temp data to reload
            tdb_tmp.load_data('data.json')      #actual data
            data = tdb_tmp.get_data(ent_id)
            self.set_data(ent_id,data)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
