import cherrypy
import json
import os.path

from _tobacco_database import _tobacco_database

class YearController(object):

	def __init__(self, tdb=None):
		if tdb is None:
			self.tdb = _movie_database()
		else:
			self.tdb = tdb
		self.tdb.load_data('data.json')

	def GET_YEARS(self):					#get all data for all years
		output = {'result': 'success'}

		try:
			for y in range(2015, 2018): #year range
				yw=self.tdb.filter_year_val(y,'want')		#different types of data
				ya=self.tdb.filter_year_val(y,'attempt')
				ys=self.tdb.filter_year_val(y,'smoke')
				ysl=self.tdb.filter_year_val(y,'smokeless')

				i=0
				s=0							#get average of each data type
				for k,v in yw.items():
					i+=1
					s+=v
				avg=s/i

				output[str(y) + ' want']=round(avg, 2)

				i=0
				s=0
				for k,v in ya.items():
					i+=1
					s+=v
				avg=s/i

				output[str(y) + ' attempt']=round(avg, 2)

				i=0
				s=0
				for k,v in ys.items():
					i+=1
					s+=v
				avg=s/i

				output[str(y) + ' smoke']=round(avg, 2)

				i=0
				s=0
				for k,v in ysl.items():
					i+=1
					s+=v
				avg=s/i

				output[str(y) + ' smokeless']=round(avg, 2)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)



	def GET_YEAR(self, year):				#get data for all years
		output = {'result': 'success'}
		ent_id = str(year.replace("_"," "))
		# The URL may contain additional filters in the form of /state/Alabama:gender=Female:year=2016
		stack = ent_id.split(":") # List of each filter and its value
		year = int(stack[0]) # The target year is the first argument
		del stack[0] # Delete it once it's extracted

		try:
			yw=self.tdb.filter_year_val(year,'want')		#different types of data
			ya=self.tdb.filter_year_val(year,'attempt')
			ys=self.tdb.filter_year_val(year,'smoke')
			ysl=self.tdb.filter_year_val(year,'smokeless')

			# Additionally call filter functions for each of the stacked filter types in order to select the targeted group
			for ids in stack:
				filters = ids.split("=")

				if filters[0] == "gender":
					yw = self.tdb.filter_gender_val(filters[1], "want", yw)
					ya = self.tdb.filter_gender_val(filters[1], "attempt", ya)
					ys = self.tdb.filter_gender_val(filters[1], "smoke", ys)
					ysl = self.tdb.filter_gender_val(filters[1], "smokeless", ysl)
				if filters[0] == "education":
					f = filters[1]	
					yw = self.tdb.filter_education_val(f, "want", yw)
					ya = self.tdb.filter_education_val(f, "attempt", ya)
					ys = self.tdb.filter_education_val(f, "smoke", ys)
					ysl = self.tdb.filter_education_val(f, "smokeless", ysl)
				if filters[0] == "state":
					f = filters[1]
					yw = self.tdb.filter_state_val(f, "want", yw)
					ya = self.tdb.filter_state_val(f, "attempt", ya)
					ys = self.tdb.filter_state_val(f, "smoke", ys)
					ysl = self.tdb.filter_state_val(f, "smokeless", ysl)

			output['year'] = str(year)

			i=0
			s=0								#get average of each data type
			if yw:	#make sure not empty
				for k,v in yw.items():
					i+=1
					s+=v
				avg=s/i

				output['want']=round(avg, 2)

			i=0
			s=0
			if ya:
				for k,v in ya.items():
					i+=1
					s+=v
				avg=s/i

				output['attempt']=round(avg, 2)

			i=0
			s=0
			if ys:
				for k,v in ys.items():
					i+=1
					s+=v
				avg=s/i

				output['smoke']=round(avg, 2)

			i=0
			s=0
			if ysl:
				for k,v in ysl.items():
					i+=1
					s+=v
				avg=s/i

				output['smokeless']=round(avg, 2)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)



	def DEL_YEAR(self, year):			#delete data for a certain year
		output = {'result': 'success'}
		ent_id = str(year.replace("_", " "))
		filters = ent_id.split(":")
		year = int(filters[0]) # Extract the year and delete all its entries

		try:
			data=self.tdb.filter_year_val(year)

			for k,v in data.items():
					self.tdb.delete_entry(k)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)


		return json.dumps(output)








