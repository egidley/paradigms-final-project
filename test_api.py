from _tobacco_database import _tobacco_database
import unittest

class TestTobaccoDatabase(unittest.TestCase):

	tdb = _tobacco_database()

	def reset_data(self):			#reset before each test
		self.tdb.delete_all()
		self.tdb.load_data('data.json')

	def test_filter_gender_val(self):
		self.reset_data()
		gen=self.tdb.filter_gender_val('Male')				#Test with question type
		self.assertEqual(gen[3], 2.9)
		gen=self.tdb.filter_gender_val('Female', 'smoke')	#Test without question type
		self.assertEqual(gen[6], 1.9)

	def test_filter_education_val(self):
		self.reset_data()
		edu=self.tdb.filter_education_val('Middle School')					#Test with question type
		self.assertEqual(edu[0], 2.4)
		edu=self.tdb.filter_education_val('Middle School', 'smokeless')		#Test without question type
		self.assertEqual(edu[9], 1.5)

	def test_filter_year_val(self):
		self.reset_data()
		year=self.tdb.filter_year_val(2017)					#Test with question type
		self.assertEqual(year[0], 2.4)
		year=self.tdb.filter_year_val(2017, 'smokeless')	#Test without question type
		self.assertEqual(year[9], 1.5)

	def test_filter_state_val(self):
		self.reset_data()
		state=self.tdb.filter_state_val('Arizona')		#Test with question type
		self.assertEqual(state[0], 2.4)
		state=self.tdb.filter_state_val('Connecticut')	#Test without question type
		self.assertEqual(state[18], 3.5)

	def test_delete_entry(self):
		self.reset_data()			#Test Delete
		self.tdb.delete_entry(3)
		data=self.tdb.get_id(3)
		self.assertEqual(data, None)


if __name__ == "__main__":
    unittest.main()
