#data.py
#Paradigms Final Project

import cherrypy
import re, json
from _tobacco_database import _tobacco_database

class DataController(object):
    def __init__(self, tdb=None):
        if tdb is None:
            self.tdb = _tobacco_database()
        else:
            self.tdb = tdb

        self.tdb.load_data('data.json')

    def GET_ALL(self):                      #gets all the data in the set
        output = {'result':'success'}
        data_ids = self.tdb.get_all_ids()
        data_info = list()
        for d in data_ids:
            outdat = dict()
            outdat = self.tdb.get_data(d)
            outdat['id'] = d
            outdat['result'] = 'success'
            data_info.append(outdat)
        output['data'] = data_info
        return json.dumps(output)

    def GET_ID(self,ent_id):                #gets the data for the provided id
        output = {'result':'success'}
        ent_id = int(ent_id)
        try:
            data = self.tdb.get_data(ent_id)
            if data is not None:
                output = data
            else:
                output['result'] = 'error'
                output['message'] = 'ID not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def PUT_ID(self,ent_id):            #puts data given an id
        output = {'result':'success'}
        ent_id = int(ent_id)
        data = json.loads(cherrypy.request.body.read())
        try:
            info_d = dict()
            for k in data:
                info_d[k] = data[k]
            self.tdb.set_data(ent_id, info_d)   #function to set new data
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def POST_DATA(self):                #Posts data
        output = {'result':'success'}

        data = json.loads(cherrypy.request.body.read())
        try:
            info_d = dict()
            for k in data:
                info_d[k] = data[k]
            #find last used ID and get next number
            id_list = sorted(self.tdb.get_all_ids())
            last_id = id_list[-1]
            ent_id = int(last_id) + 1
            self.tdb.set_data(ent_id, info_d)
            output['id'] = ent_id
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)    

    def DELETE_ALL(self):               #deletes all data
        output = {'result':'success'}
        self.tdb.delete_all()
        return json.dumps(output)

    def DELETE_ID(self,ent_id):         #deletes data for a specific id
        output = {'result':'success'}
        ent_id = int(ent_id)
        try:
            self.tdb.delete_entry(ent_id)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
