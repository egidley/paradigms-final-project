import sys
import json

# QUESTIONS: Percent of Current Smokers Who Want to Quit ('want'),
#            Quit Attempt in Past Year Among Current Cigarette Smokers ('attempt'),
#            Smoker Status ('smoke'),
#            User Status ('smokeless')

class _tobacco_database:
    def __init__(self):
        self.educations = dict()
        self.genders = dict()
        self.years = dict()
        self.locations = dict()
        self.measure_descriptions = dict()
        self.sample_sizes = dict()
        self.std_errors = dict()
        self.low_confs = dict()
        self.high_confs = dict()
        self.values = dict()
    
    def load_data(self, data_file):
        f = open(data_file)
        self.alldata = json.load(f)
        self.data = dict()
        id_counter = 0
        #create dicts for each component of the data
        for entry in self.alldata:
            if 'data_value' in entry:
                self.data[id_counter] = entry
                if 'education' in entry:
                    self.educations[id_counter] = entry['education']
                if 'gender' in entry:
                    self.genders[id_counter] = entry['gender']
                if 'year' in entry:
                    self.years[id_counter] = int(entry['year'].strip())
                if 'locationdesc' in entry:
                    self.locations[id_counter] = entry['locationdesc']
                if 'measuredesc' in entry:
                    self.measure_descriptions[id_counter] = entry['measuredesc']
                if 'sample_size' in entry:
                    self.sample_sizes[id_counter] = int(entry['sample_size'])
                if 'data_value_std_err' in entry:
                    self.std_errors[id_counter] = float(entry['data_value_std_err'])
                if 'low_confidence_limit' in entry:
                    self.low_confs[id_counter] = float(entry['low_confidence_limit'])
                if 'high_confidence_limit' in entry:
                    self.high_confs[id_counter] = float(entry['high_confidence_limit'])
                #value or percentage (data) is required
                self.values[id_counter] = float(entry['data_value'])
                id_counter += 1

    #return all data
    def dump_data(self):
        return self.data

    #returns a dict of data for specified ID
    def get_data(self, ent_id):
        if ent_id in self.data:
            return self.data[ent_id]
        else:
            return None

    #returns a list of all ids
    def get_all_ids(self):
        ids = list()
        for k in self.data.keys():
            ids.append(k)
        return ids
    
    #filter functions
    #returns percentages for each measured dataset
    def return_values(self, question, ids, k):
        if question == 'None':
            ids[k] = self.values[k]
        elif question == 'want' and self.measure_descriptions[k] == 'Percent of Current Smokers Who Want to Quit':
            ids[k] = self.values[k]
        elif question == 'attempt' and self.measure_descriptions[k] == 'Quit Attempt in Past Year Among Current Cigarette Smokers':
            ids[k] = self.values[k]
        elif question == 'smoke' and self.measure_descriptions[k] == 'Smoking Status':
            ids[k] = self.values[k]
        elif question == 'smokeless' and self.measure_descriptions[k] == 'User Status':
            ids[k] = self.values[k]
        

    #returns list of IDs that match specified gender
    def filter_gender_val(self, g, question='None', data=None):
        ids = dict()
        if data is None:
            data = self.values
        for k, v in self.genders.items():
            if v == g and k in data:
                self.return_values(question, ids, k)
        return ids

    #returns list of IDs that match specified education
    def filter_education_val(self, edu, question='None', data=None):
        ids = dict()
        if data is None:
            data = self.values
        for k,v in self.educations.items():
            if v == edu and k in data:
                self.return_values(question, ids, k)
        return ids

    #returns list of IDs that match specified year
    def filter_year_val(self, yr, question='None', data=None):
        ids = dict()
        if data is None:
            data = self.values
        for k,v in self.years.items():
            if v == yr and k in data:
                self.return_values(question, ids, k)
        return ids

    #returns dict of IDs with values that match specified state
    def filter_state_val(self, st, question='None', data=None):
        ids = dict()
        if data is None:
            data = self.values
        for k,v in self.locations.items():
            if v == st and k in data:
                self.return_values(question, ids, k)
        return ids

    #create a new entry
    def set_data(self, ent_id, info_d):
        for key in info_d:
            if 'education'==key:
                self.educations[ent_id] = info_d['education']
            if 'gender'==key:
                self.genders[ent_id] = info_d['gender']
            if 'year'==key:
                self.years[ent_id] = info_d['year']
            if 'locationdesc'==key:
                self.locations[ent_id] = info_d['locationdesc']
            if 'measuredesc'==key:
                self.measure_descriptions[ent_id] = info_d['measuredesc']
            if 'sample_size'==key:
                self.sample_sizes[ent_id] = info_d['sample_size']
            if 'data_value_std_err'==key:
                self.std_errors[ent_id] = info_d['std_errors']
            if 'low_confidence_limit'==key:
                self.low_confs[ent_id] = info_d['low_confidence_limit']
            if 'high_confidence_limit'==key:
                self.high_confs[ent_id] = info_d['high_confidence_limit']

        if ent_id in self.data:
            self.data[ent_id].update(info_d)
        else:
            self.data[ent_id] = info_d
        self.values[ent_id] = info_d['data_value']

    #clear each dict
    def delete_all(self):
        self.educations = dict()
        self.genders = dict()
        self.years = dict()
        self.locations = dict()
        self.measure_descriptions = dict()
        self.sample_sizes = dict()
        self.std_errors = dict()
        self.low_confs = dict()
        self.high_confs = dict()
        self.values = dict()
        self.data = dict()

    #delete a single entry by ID
    def delete_entry(self, ent_id):
        if self.data[ent_id]:
            del self.data[ent_id]
        if self.educations[ent_id]:
            del self.educations[ent_id]
        if self.genders[ent_id]:
            del self.genders[ent_id]
        if self.years[ent_id]:
            del self.years[ent_id]
        if self.locations[ent_id]:
            del self.locations[ent_id]
        if self.measure_descriptions[ent_id]:
            del self.measure_descriptions[ent_id]
        if self.sample_sizes[ent_id]:
            del self.sample_sizes[ent_id]
        if self.std_errors[ent_id]:
            del self.std_errors[ent_id]
        if self.low_confs[ent_id]:
            del self.low_confs[ent_id]
        if self.high_confs[ent_id]:
            del self.high_confs[ent_id]
        if self.values[ent_id]:
            del self.values[ent_id]


if __name__ == '__main__':
    tdb = _tobacco_database()

    tdb.load_data('data.json')
    ids = tdb.get_all_ids()
    #testing:
    #for i in ids:
    #    print(tdb.get_data(i))
