#main.py
#Server for final project

import cherrypy

from data import DataController
from years import YearController
from reset import ResetController
from state import StateController
from gender import GenderController
from education import EducationController


from _tobacco_database import _tobacco_database

class optionsController:
    def OPTIONS(self,*args,**kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "*"

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    #instantiate database
    tdb_o = _tobacco_database()

    #instantiate controllers
    dataController = DataController(tdb=tdb_o)
    resetController = ResetController(tdb=tdb_o)
    yearController = YearController(tdb=tdb_o)
    stateController = StateController(tdb=tdb_o)
    genderController = GenderController(tdb=tdb_o)
    educationController = EducationController(tdb=tdb_o)

    #connect data resources
    dispatcher.connect('get_all','/data/', controller=dataController, action = 'GET_ALL', conditions=dict(method=['GET']))
    dispatcher.connect('get_id','/data/:ent_id', controller=dataController, action = 'GET_ID', conditions=dict(method=['GET']))
    dispatcher.connect('put_id','/data/:ent_id', controller=dataController, action = 'PUT_ID', conditions=dict(method=['PUT']))
    dispatcher.connect('post_data','/data/', controller=dataController, action = 'POST_DATA', conditions=dict(method=['POST']))
    dispatcher.connect('delete_all','/data/', controller=dataController, action = 'DELETE_ALL', conditions=dict(method=['DELETE']))
    dispatcher.connect('delete_by_id','/data/:ent_id', controller=dataController, action = 'DELETE_ID', conditions=dict(method=['DELETE']))
    dispatcher.connect('reset_all','/reset/', controller=resetController, action = 'RESET_DATA', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_id','/reset/:ent_id', controller=dataController, action = 'RESET_ID', conditions=dict(method=['PUT']))
    dispatcher.connect('get_year', '/year/:year', controller=yearController, action = 'GET_YEAR', conditions=dict(method=['GET']))
    dispatcher.connect('get_years', '/year/', controller=yearController, action = 'GET_YEARS', conditions=dict(method=['GET']))
    dispatcher.connect('del_year', '/year/:year', controller=yearController, action = 'DEL_YEAR', conditions=dict(method=['DELETE']))
    dispatcher.connect('state_all','/state/', controller=stateController, action = 'GET_ALL', conditions=dict(method=['GET']))
    dispatcher.connect('state_id','/state/:ent_id', controller=stateController, action = 'GET_ID', conditions=dict(method=['GET']))
    dispatcher.connect('del_state_id','/state/:ent_id', controller=stateController, action = 'DELETE_ID', conditions=dict(method=['DELETE']))
    dispatcher.connect('gender_all','/gender/', controller=genderController, action = 'GET_ALL', conditions=dict(method=['GET']))
    dispatcher.connect('gender_id','/gender/:ent_id', controller=genderController, action = 'GET_ID', conditions=dict(method=['GET']))
    dispatcher.connect('del_gender_id','/gender/:ent_id', controller=genderController, action = 'DELETE_ID', conditions=dict(method=['DELETE']))
    dispatcher.connect('education_all','/education/', controller=educationController, action = 'GET_ALL', conditions=dict(method=['GET']))
    dispatcher.connect('education_id','/education/:ent_id', controller=educationController, action = 'GET_ID', conditions=dict(method=['GET']))
    dispatcher.connect('del_education_id','/education/:ent_id', controller=educationController, action = 'DELETE_ID', conditions=dict(method=['DELETE']))

    dispatcher.connect('data_opt','/data/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('data_opt_id','/data/:ent_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))	
    dispatcher.connect('reset_opt','/reset/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_opt_id','/reset/:ent_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('year_opt_all','/year/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('year_opt','/year/:year', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('state_opt_all','/state/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('state_opt','/state/:ent_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('gender_opt_all','/gender/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('gender_opt','/gender/:ent_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('edu_opt_all','/education/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('edu_opt','/education/:ent_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    #configuration
    conf = {
        'global' : {
            'server.socket_host' : 'student04.cse.nd.edu',
            'server.socket_port' : 52063,
            },
        '/':{
            'request.dispatch' : dispatcher,
            'tools.CORS.on' : True
            }
        }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
