chergenr
egidley
lkusky
mfinnan

###################### Paradigms Final Project - Milestone 2 ########################

Our tobacco_database.py file has multiple functions that allow the data to be used efficiently. 
There are filter functions for the major characteristics of each study, get all data, delete 
all, delete a certain data ID, and a set function.

To run the test file, one simply runs the command "python3 test_api.py".

###################### Paradigms Final Project - Milestone 3 ########################

Our web service uses port 52063.  It can be used by the customer 
to acquire and edit certain data. For example, the customer can
use it to get all the data or just some filtered by year or state.
One can also use it to delete certain data or add data.

To run the server, run "python3 main.py".

To run all test scripts, after starting the server, in a different terminal run "python3 test_ws.py".

###################### Paradigms Final Project - Milestone 4 ########################

Our web client uses information from our server to show the information for desired filtered data.
The user can select options from any of the 5 dropdown menus in order to customize the information.
Once the data is filtered, the percent of people matching the filters is displayed at the bottom of 
the page. The map is also updated to show the data trends for the specific group.

This resource can be very useful in analyzing tobacco trends in the US, by being able to see what 
demographics have used tobacco and which ones are trying to or have attempted to quit.

To utilize the web client, start the server by using the command "python3 main.py". Then go to the URL 
student04.cse.nd.edu/chergenr/paradigms-final-project/client_1 and select filters to update the data.

To test that the client works accurately, we calculated the percent for multiple filter settings to
ensure that the outputs were correct. Since there are only four maps, we tested each "question" type
to make sure the right map updates. Because we had already tested our data (see server tests from 
Milestone 3), we checked that the data we displayed and the trends on the map matched our server data.

Git repository: 
https://gitlab.com/egidley/paradigms-final-project
